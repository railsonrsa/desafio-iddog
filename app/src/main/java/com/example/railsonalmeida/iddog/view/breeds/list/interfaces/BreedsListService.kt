package com.example.railsonalmeida.iddog.view.register.interfaces

import com.example.railsonalmeida.iddog.data.response.FeedResponse
import io.reactivex.Observable

/**
 * Created by railsonalmeida on 23/04/2018.
 */

interface BreedsListService {

    fun loadFeed(query: String?, token: String): Observable<FeedResponse>
}