package com.example.railsonalmeida.iddog.app

import android.app.Application
import com.example.railsonalmeida.iddog.app.di.AppComponent
import com.example.railsonalmeida.iddog.app.di.AppModule
import com.example.railsonalmeida.iddog.app.di.DaggerAppComponent
import com.example.railsonalmeida.iddog.view.register.di.*

/**
 * Created by railsonalmeida on 23/04/2018.
 */

class IDdogApplication: Application() {

    lateinit var appComponent: AppComponent
    lateinit var registerComponent: RegisterComponent
    lateinit var breedsListComponent: BreedsListComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()

        registerComponent = appComponent.inject(RegisterModule())
        breedsListComponent = appComponent.inject(BreedsListModule())

    }

}