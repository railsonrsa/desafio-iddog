package com.example.railsonalmeida.iddog.view.register

import br.com.railsonrsa.nubank_hiring_exercise.schedulers.SchedulerProvider
import com.example.railsonalmeida.iddog.R
import com.example.railsonalmeida.iddog.app.base.BaseView
import com.example.railsonalmeida.iddog.app.utils.AuthUtils
import com.example.railsonalmeida.iddog.data.request.RegisterRequest
import com.example.railsonalmeida.iddog.data.response.ErrorResponse
import com.example.railsonalmeida.iddog.data.response.UserResponse
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterPresenter
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterService
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterView
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

/**
 * Created by railsonalmeida on 23/04/2018.
 */

class RegisterPresenterImpl(var view: RegisterView?,
                            val service: RegisterService,
                            val schedulers: SchedulerProvider): RegisterPresenter {

    override fun setView(view: BaseView) {
        this.view = view as RegisterView
    }

    override fun registerUser(email: String) {
        if (email.isNotBlank()) {
            view?.disableRegisterButton()
            view?.startLoading()

            val registerRequest = RegisterRequest(email)
            service.registerUser(registerRequest)
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())
                    .subscribe(
                            {response: UserResponse? -> onSuccessRegisterUser(response) },
                            {error -> onErrorRegisterError(error as HttpException)}
                    )
        } else {
            // Emails is not valid (Either is blank or empty/null)
            view?.onInvalidEmail()
        }
    }

    override fun onSuccessRegisterUser(response: UserResponse?) {
        val context = view?.getContext()
        AuthUtils.saveUsersToken(context, response?.user?.token)
        view?.enableRegisterButton()
        view?.stopLoading()
        view?.onSuccessRegister()
    }

    override fun onErrorRegisterError(error: HttpException?) {
        if (error?.code() == 400) {
            val gson = Gson()
            val errorResponse = gson.fromJson(error.response()?.errorBody()?.string(), ErrorResponse::class.java)
            view?.showMessageError(errorResponse.error.message)
        } else {
            view?.showMessageError(view?.getContext()?.getString(R.string.error)?.toString())
        }
        view?.enableRegisterButton()
        view?.stopLoading()
    }
}