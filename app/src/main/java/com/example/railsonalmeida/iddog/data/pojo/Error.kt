package com.example.railsonalmeida.iddog.data.pojo

import com.google.gson.annotations.SerializedName

/**
 * Created by railsonalmeida on 24/04/2018.
 */

data class Error(@SerializedName("message") val message: String)