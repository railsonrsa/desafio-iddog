package com.example.railsonalmeida.iddog.view.register.interfaces

import com.example.railsonalmeida.iddog.app.base.BasePresenter
import com.example.railsonalmeida.iddog.data.response.UserResponse
import retrofit2.HttpException

/**
 * Created by railsonalmeida on 23/04/2018.
 */

interface RegisterPresenter: BasePresenter {

    fun registerUser(email: String)

    fun onSuccessRegisterUser(response: UserResponse?)

    fun onErrorRegisterError(error: HttpException?)

}