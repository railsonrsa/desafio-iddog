package com.example.railsonalmeida.iddog.view.register

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.railsonalmeida.iddog.R
import com.example.railsonalmeida.iddog.app.base.BaseFragment
import com.example.railsonalmeida.iddog.view.breeds.breed.interfaces.BreedActivityCallback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_breed.*

/**
 * Created by railsonalmeida on 23/04/2018.
 */

class BreedFragment: BaseFragment() {

    private lateinit var activityCallback: BreedActivityCallback

    private var uri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityCallback = context as BreedActivityCallback
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_breed, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uri = activityCallback.getUri()
        Picasso.with(this.context).load(uri).into(iv_breed)
    }

}