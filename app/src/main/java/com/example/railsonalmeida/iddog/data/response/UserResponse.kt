package com.example.railsonalmeida.iddog.data.response

import com.example.railsonalmeida.iddog.data.pojo.User
import com.google.gson.annotations.SerializedName

/**
 * Created by railsonalmeida on 23/04/2018.
 */

data class UserResponse(@SerializedName("user") var user: User)