package com.example.railsonalmeida.iddog.app.di

import com.example.railsonalmeida.iddog.BuildConfig
import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListApi
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by railsonalmeida on 23/04/2018.
 */

@Module
class ApiModule {

    @Provides
    @Singleton
    @Named("baseUrl")
    fun provideBaseUrl(): String {
        return BuildConfig.BASE_URL
    }

    @Provides
    fun provideRegisterApi(retrofit: Retrofit): RegisterApi {
        return retrofit.create(RegisterApi::class.java)
    }

    @Provides
    fun provdeBreedsListApi(retrofit: Retrofit): BreedsListApi {
        return retrofit.create(BreedsListApi::class.java)
    }

}