package com.example.railsonalmeida.iddog.view.register

import android.content.Intent
import android.os.Bundle
import com.example.railsonalmeida.iddog.R
import com.example.railsonalmeida.iddog.app.base.BaseActivity
import com.example.railsonalmeida.iddog.app.utils.AuthUtils
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterActivityCallback

class RegisterActivity : BaseActivity(), RegisterActivityCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        // Verify if user has a token saved
        if (AuthUtils.verifyUsersToken(this)) {
            // Go to breeds list
            onSuccessRegister()
        } else {
            // Load registration screen
            loadFragment()
        }
    }

    override fun loadFragment() {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.container, RegisterFragment())
                .commit()
    }

    override fun onSuccessRegister() {
        val intent = Intent(this, BreedsListActivity::class.java)
        startActivity(intent)
    }
}
