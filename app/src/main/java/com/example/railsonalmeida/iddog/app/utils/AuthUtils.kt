package com.example.railsonalmeida.iddog.app.utils

import android.content.Context
import com.example.railsonalmeida.iddog.R

/**
 * Created by railsonalmeida on 23/04/2018.
 */

class AuthUtils {

    companion object {

        // Verify if user has a token stored, if so, redirect to filter. Otherwise, show register
        fun verifyUsersToken(context: Context): Boolean {
            val sharedPreferences = context.getSharedPreferences(context.getString(R.string.token),Context.MODE_PRIVATE)
            val token = sharedPreferences.getString(context.getString(R.string.token), "")

            if (token.isNotEmpty()) {
                return true
            }
            return false
        }

        // Saves users token
        fun saveUsersToken(context: Context?, token: String?) {
            val sharedPreferences = context?.getSharedPreferences(context.getString(R.string.token),Context.MODE_PRIVATE)
            with(sharedPreferences?.edit()) {
                this?.putString(context?.getString(R.string.token), token)
                this?.commit()
            }
        }

        fun getUsersToken(context: Context): String {
            val sharedPreferences = context.getSharedPreferences(context.getString(R.string.token),Context.MODE_PRIVATE)
            val token = sharedPreferences.getString(context.getString(R.string.token), "")
            return token
        }
    }



}