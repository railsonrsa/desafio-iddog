package com.example.railsonalmeida.iddog.app.base

/**
 * Created by railsonalmeida on 23/04/2018.
 */

interface BasePresenter {

    fun setView(view: BaseView)

}