package com.example.railsonalmeida.iddog.view.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.railsonalmeida.iddog.R
import com.example.railsonalmeida.iddog.app.IDdogApplication
import com.example.railsonalmeida.iddog.app.base.BaseFragment
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterActivityCallback
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterPresenter
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterView
import kotlinx.android.synthetic.main.fragment_register.*
import javax.inject.Inject

/**
 * Created by railsonalmeida on 23/04/2018.
 */

class RegisterFragment: BaseFragment(), RegisterView {

    @Inject lateinit var presenter: RegisterPresenter
    lateinit var activityCallback: RegisterActivityCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (context.applicationContext as IDdogApplication).registerComponent.inject(this)
        activityCallback = context as RegisterActivityCallback
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.setView(this)

        btn_register.setOnClickListener {
            presenter.registerUser(edt_email.text.toString())
        }
    }

    override fun onSuccessRegister() {
        activityCallback.onSuccessRegister()
    }

    override fun showMessageError(message: String?) {
        val toast = Toast.makeText(context, message, Toast.LENGTH_LONG)
        toast.show()
    }

    override fun onInvalidEmail() {
        edt_email.error = getString(R.string.invalid_email)
    }

    override fun enableRegisterButton() {
        btn_register.isEnabled = true
    }

    override fun disableRegisterButton() {
        btn_register.isEnabled = false
    }

    override fun startLoading() {
        loading.visibility = View.VISIBLE
    }

    override fun stopLoading() {
        loading.visibility = View.INVISIBLE
    }

}