package com.example.railsonalmeida.iddog.view.register.di

import com.example.railsonalmeida.iddog.view.register.BreedsListPresenterImpl
import com.example.railsonalmeida.iddog.view.register.BreedsListServiceImpl
import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListApi
import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListPresenter
import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListService
import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListView
import dagger.Module
import dagger.Provides

/**
 * Created by railsonalmeida on 23/04/2018.
 */

@Module
class BreedsListModule {

    @Provides
    fun providePresenter(service: BreedsListService): BreedsListPresenter {
        return BreedsListPresenterImpl(null, service)
    }

    @Provides
    fun provideService(api: BreedsListApi): BreedsListService {
        return BreedsListServiceImpl(api)
    }
}