package com.example.railsonalmeida.iddog.app.di

import android.app.Application
import android.content.Context
import com.example.railsonalmeida.iddog.R
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by railsonalmeida on 23/04/2018.
 */

@Module
class AppModule(val application: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return application
    }

}