package com.example.railsonalmeida.iddog.view.register.interfaces

import com.example.railsonalmeida.iddog.data.request.RegisterRequest
import com.example.railsonalmeida.iddog.data.response.UserResponse
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by railsonalmeida on 23/04/2018.
 */

interface RegisterApi {

    @POST("/signup")
    fun registerUser(@Body request: RegisterRequest): Observable<UserResponse>
}