package com.example.railsonalmeida.iddog.view.register.di

import com.example.railsonalmeida.iddog.app.di.PerActivity
import com.example.railsonalmeida.iddog.view.register.BreedsListFragment
import dagger.Subcomponent

/**
 * Created by railsonalmeida on 23/04/2018.
 */

@PerActivity
@Subcomponent(modules = [BreedsListModule::class])
interface BreedsListComponent {

    fun inject(breedsListFragment: BreedsListFragment)
}