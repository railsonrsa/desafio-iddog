package com.example.railsonalmeida.iddog.view.register.interfaces

/**
 * Created by railsonalmeida on 24/04/2018.
 */

interface RegisterActivityCallback {

    fun onSuccessRegister()
}