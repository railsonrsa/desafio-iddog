package com.example.railsonalmeida.iddog.view.register.interfaces

import com.example.railsonalmeida.iddog.data.request.RegisterRequest
import com.example.railsonalmeida.iddog.data.response.UserResponse
import io.reactivex.Observable

/**
 * Created by railsonalmeida on 23/04/2018.
 */

interface RegisterService {

    fun registerUser(request: RegisterRequest): Observable<UserResponse>
}