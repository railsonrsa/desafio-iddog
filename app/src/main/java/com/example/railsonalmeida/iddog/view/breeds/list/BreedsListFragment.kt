package com.example.railsonalmeida.iddog.view.register

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.railsonalmeida.iddog.R
import com.example.railsonalmeida.iddog.app.IDdogApplication
import com.example.railsonalmeida.iddog.app.base.BaseFragment
import com.example.railsonalmeida.iddog.app.utils.AuthUtils
import com.example.railsonalmeida.iddog.view.breeds.list.adapters.BreedsListAdapter
import com.example.railsonalmeida.iddog.view.breeds.list.interfaces.BreedsListActivityCallback
import com.example.railsonalmeida.iddog.view.breeds.list.interfaces.OnBreedClickListener
import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListPresenter
import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListView
import kotlinx.android.synthetic.main.fragment_breeds_list.*
import javax.inject.Inject

/**
 * Created by railsonalmeida on 23/04/2018.
 */

class BreedsListFragment: BaseFragment(), BreedsListView {

    @Inject lateinit var presenter: BreedsListPresenter
    lateinit var activityCallback: BreedsListActivityCallback

    private var adapter: BreedsListAdapter? = null
    private var data: List<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        (context.applicationContext as IDdogApplication).breedsListComponent.inject(this)
        activityCallback = context as BreedsListActivityCallback
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_breeds_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.setView(this)
        presenter.loadFeed(null, AuthUtils.getUsersToken(activity))
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId) {
            R.id.husky -> {
                presenter.loadFeed(getString(R.string.husky).toLowerCase(), AuthUtils.getUsersToken(activity))
                return true
            }
            R.id.pug -> {
                presenter.loadFeed(getString(R.string.pug).toLowerCase(), AuthUtils.getUsersToken(activity))
                return true
            }
            R.id.hound -> {
                presenter.loadFeed(getString(R.string.hound).toLowerCase(), AuthUtils.getUsersToken(activity))
                return true
            }
            R.id.labrador -> {
                presenter.loadFeed(getString(R.string.labrador).toLowerCase(), AuthUtils.getUsersToken(activity))
                return true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun loadFeed(list: List<String>) {
        data = list
        if (adapter == null) {
            adapter = BreedsListAdapter(context, list, object : OnBreedClickListener {
                override fun onBreedClickListener(imageUrl: String?) {
                    activityCallback.openBreedImage(imageUrl)
                }
            })
            rv_breeds.adapter = adapter
            rv_breeds.layoutManager = GridLayoutManager(context, 2)
        } else {
            adapter?.setData(list)
            adapter?.notifyDataSetChanged()
        }
    }

    override fun startLoading() {
        loading.visibility = View.VISIBLE
    }

    override fun stopLoading() {
        loading.visibility = View.GONE
    }

    override fun hideFeed() {
        rv_breeds.visibility = View.INVISIBLE
    }

    override fun showFeed() {
        rv_breeds.visibility = View.VISIBLE
    }

    override fun showMessageError(message: String?) {
        val toast = Toast.makeText(context, message, Toast.LENGTH_LONG)
        toast.show()
    }
}