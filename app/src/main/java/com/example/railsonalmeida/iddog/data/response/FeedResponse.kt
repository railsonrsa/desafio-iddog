package com.example.railsonalmeida.iddog.data.response

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by railsonalmeida on 25/04/2018.
 */

data class FeedResponse(@SerializedName("category") val category: String,
                        @SerializedName("list") val list: Array<String>) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FeedResponse

        if (category != other.category) return false
        if (!Arrays.equals(list, other.list)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = category.hashCode()
        result = 31 * result + Arrays.hashCode(list)
        return result
    }
}