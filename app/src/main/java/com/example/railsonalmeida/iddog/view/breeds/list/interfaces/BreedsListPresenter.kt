package com.example.railsonalmeida.iddog.view.register.interfaces

import com.example.railsonalmeida.iddog.app.base.BasePresenter
import com.example.railsonalmeida.iddog.data.response.FeedResponse
import retrofit2.HttpException

/**
 * Created by railsonalmeida on 23/04/2018.
 */

interface BreedsListPresenter: BasePresenter {

    fun loadFeed(query: String?, token: String)

    fun onSuccessLoadFeed(feedResponse: FeedResponse?)

    fun onErrorLoadFeed(error: HttpException)
}