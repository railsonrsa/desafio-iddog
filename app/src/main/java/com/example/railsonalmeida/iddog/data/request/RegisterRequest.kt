package com.example.railsonalmeida.iddog.data.request

import com.google.gson.annotations.SerializedName

/**
 * Created by railsonalmeida on 23/04/2018.
 */

data class RegisterRequest(@SerializedName("email") var email: String)