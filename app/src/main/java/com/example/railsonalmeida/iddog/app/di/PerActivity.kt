package com.example.railsonalmeida.iddog.app.di

import javax.inject.Scope

/**
 * Created by railsonalmeida on 23/04/2018.
 */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity