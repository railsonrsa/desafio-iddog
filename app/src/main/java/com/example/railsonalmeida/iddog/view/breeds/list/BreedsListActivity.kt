package com.example.railsonalmeida.iddog.view.register

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import com.example.railsonalmeida.iddog.R
import com.example.railsonalmeida.iddog.app.base.BaseActivity
import com.example.railsonalmeida.iddog.view.breeds.list.interfaces.BreedsListActivityCallback

class BreedsListActivity : BaseActivity(), BreedsListActivityCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_breeds_list)
        loadFragment()
    }

    override fun loadFragment() {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.container, BreedsListFragment())
                .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.breeds_menu, menu)
        return true
    }

    override fun openBreedImage(imageUrl: String?) {
        var intent = Intent(this, BreedActivity::class.java)
        val uri = Uri.parse(imageUrl)
        intent.data = uri
        startActivity(intent)
    }
}
