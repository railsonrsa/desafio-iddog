package com.example.railsonalmeida.iddog.data.response

import com.example.railsonalmeida.iddog.data.pojo.Error
import com.google.gson.annotations.SerializedName

/**
 * Created by railsonalmeida on 24/04/2018.
 */

data class ErrorResponse(@SerializedName("error") val error: Error)