package com.example.railsonalmeida.iddog.view.register

import com.example.railsonalmeida.iddog.data.response.FeedResponse
import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListApi
import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListService
import io.reactivex.Observable

/**
 * Created by railsonalmeida on 23/04/2018.
 */

class BreedsListServiceImpl(val api: BreedsListApi): BreedsListService {

    override fun loadFeed(query: String?, token: String): Observable<FeedResponse> {
        return api.loadFeed(query, token)
    }
}