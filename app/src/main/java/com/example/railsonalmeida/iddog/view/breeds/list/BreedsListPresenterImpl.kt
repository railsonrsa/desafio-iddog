package com.example.railsonalmeida.iddog.view.register

import com.example.railsonalmeida.iddog.R
import com.example.railsonalmeida.iddog.app.base.BaseView
import com.example.railsonalmeida.iddog.data.response.ErrorResponse
import com.example.railsonalmeida.iddog.data.response.FeedResponse
import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListPresenter
import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListService
import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListView
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

/**
 * Created by railsonalmeida on 23/04/2018.
 */

class BreedsListPresenterImpl(var view: BreedsListView?, private val service: BreedsListService): BreedsListPresenter {

    override fun setView(view: BaseView) {
        this.view = view as BreedsListView
    }

    override fun loadFeed(query: String?, token: String) {
        view?.hideFeed()
        view?.startLoading()

        service.loadFeed(query, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {feedResponse -> onSuccessLoadFeed(feedResponse) },
                        {error -> onErrorLoadFeed(error as HttpException) }
                )
    }

    override fun onSuccessLoadFeed(feedResponse: FeedResponse?) {
        val list = feedResponse?.list?.toList()
        if (list != null && list.isNotEmpty()) {
            view?.loadFeed(list)
        }
        view?.stopLoading()
        view?.showFeed()
    }

    override fun onErrorLoadFeed(error: HttpException) {
        when(error.code()) {
            401 -> {
                val gson = Gson()
                val errorResponse = gson.fromJson(error.response()?.errorBody()?.string(), ErrorResponse::class.java)
                view?.showMessageError(errorResponse.error.message)
            }
            else -> {
                view?.showMessageError(view?.getContext()?.getString(R.string.error)?.toString())
            }
        }
    }
}