package com.example.railsonalmeida.iddog.data.pojo

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by railsonalmeida on 23/04/2018.
 */

data class User(@SerializedName("email") var email: String,
                @SerializedName("_id") val id: String,
                @SerializedName("token") val token: String,
                @SerializedName("createdAt") val createdAt: Date,
                @SerializedName("updatedAt") val updatedAt: Date,
                @SerializedName("__v") val v: Int)