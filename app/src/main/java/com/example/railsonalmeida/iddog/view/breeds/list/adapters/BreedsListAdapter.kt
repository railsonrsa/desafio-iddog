package com.example.railsonalmeida.iddog.view.breeds.list.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.railsonalmeida.iddog.R
import com.example.railsonalmeida.iddog.view.breeds.list.interfaces.OnBreedClickListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.breed_item_view.view.*

/**
 * Created by railsonalmeida on 26/04/2018.
 */

class BreedsListAdapter(): RecyclerView.Adapter<BreedsListHolder>() {

    private lateinit var context: Context
    private var data: List<String>? = null
    private var inflater: LayoutInflater? = null
    private var listener: OnBreedClickListener? = null

    constructor(context: Context, data: List<String>?, listener: OnBreedClickListener): this() {
        inflater = LayoutInflater.from(context)
        this.context = context
        this.data = data
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BreedsListHolder {
        val view = inflater?.inflate(R.layout.breed_item_view, parent, false)
        return BreedsListHolder(view)
    }

    override fun getItemCount(): Int {
        if (data != null && data?.size != null) {
            return data!!.size
        } else {
            return 0
        }
    }

    override fun onBindViewHolder(holder: BreedsListHolder?, position: Int) {
        val imageUrl = data?.get(position)
        Picasso.with(context).load(imageUrl).into(holder?.thumbnail)
        holder?.bind(imageUrl, listener)
    }

    fun setData(list: List<String>?) {
        data = list
    }

}

class BreedsListHolder(itemView: View?): RecyclerView.ViewHolder(itemView) {
    var thumbnail = itemView?.thumbnail

    fun bind(imageUrl: String?, listener: OnBreedClickListener?) {
        itemView.setOnClickListener {
            listener?.onBreedClickListener(imageUrl)
        }
    }
}