package com.example.railsonalmeida.iddog.view.register.di

import br.com.railsonrsa.nubank_hiring_exercise.schedulers.AppSchedulerProvider
import br.com.railsonrsa.nubank_hiring_exercise.schedulers.SchedulerProvider
import com.example.railsonalmeida.iddog.view.register.RegisterPresenterImpl
import com.example.railsonalmeida.iddog.view.register.RegisterServiceImpl
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterApi
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterPresenter
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by railsonalmeida on 23/04/2018.
 */

@Module
class RegisterModule {

    @Provides
    fun providePresenter(service: RegisterService, schedulers: SchedulerProvider): RegisterPresenter {
        return RegisterPresenterImpl(null, service, schedulers)
    }

    @Provides
    fun provideService(api: RegisterApi): RegisterService {
        return RegisterServiceImpl(api)
    }

    @Provides
    fun provideSchedulers(): SchedulerProvider {
        return AppSchedulerProvider()
    }

}