package com.example.railsonalmeida.iddog.view.register.interfaces

import android.content.Context
import com.example.railsonalmeida.iddog.app.base.BaseView

/**
 * Created by railsonalmeida on 23/04/2018.
 */

interface RegisterView: BaseView {

    fun enableRegisterButton()

    fun disableRegisterButton()

    fun startLoading()

    fun stopLoading()

    fun getContext(): Context

    fun onSuccessRegister()

    fun onInvalidEmail()

    fun showMessageError(message: String?)

}