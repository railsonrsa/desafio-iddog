package com.example.railsonalmeida.iddog.view.register

import com.example.railsonalmeida.iddog.data.request.RegisterRequest
import com.example.railsonalmeida.iddog.data.response.UserResponse
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterApi
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterService
import io.reactivex.Observable
import okhttp3.ResponseBody

/**
 * Created by railsonalmeida on 23/04/2018.
 */

class RegisterServiceImpl(val api: RegisterApi): RegisterService {

    override fun registerUser(request: RegisterRequest): Observable<UserResponse> {
        return api.registerUser(request)
    }
}