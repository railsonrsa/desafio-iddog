package com.example.railsonalmeida.iddog.app.base

import android.support.v7.app.AppCompatActivity

/**
 * Created by railsonalmeida on 23/04/2018.
 */

abstract class BaseActivity: AppCompatActivity() {

    abstract fun loadFragment()
}