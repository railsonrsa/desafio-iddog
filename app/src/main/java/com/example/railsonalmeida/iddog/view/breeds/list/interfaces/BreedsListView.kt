package com.example.railsonalmeida.iddog.view.register.interfaces

import android.content.Context
import com.example.railsonalmeida.iddog.app.base.BaseView

/**
 * Created by railsonalmeida on 23/04/2018.
 */

interface BreedsListView: BaseView {

    fun startLoading()

    fun stopLoading()

    fun loadFeed(list: List<String>)

    fun hideFeed()

    fun showFeed()

    fun showMessageError(message: String?)

    fun getContext(): Context
}