package com.example.railsonalmeida.iddog.view.breeds.breed.interfaces

import android.net.Uri

/**
 * Created by railsonalmeida on 29/04/2018.
 */

interface BreedActivityCallback {

    fun getUri(): Uri?
}