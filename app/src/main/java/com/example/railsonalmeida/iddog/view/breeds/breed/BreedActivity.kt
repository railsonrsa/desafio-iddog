package com.example.railsonalmeida.iddog.view.register

import android.net.Uri
import android.os.Bundle
import com.example.railsonalmeida.iddog.R
import com.example.railsonalmeida.iddog.app.base.BaseActivity
import com.example.railsonalmeida.iddog.view.breeds.breed.interfaces.BreedActivityCallback

class BreedActivity : BaseActivity(), BreedActivityCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_breed)
        loadFragment()
    }

    override fun loadFragment() {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.container, BreedFragment())
                .commit()
    }

    override fun getUri(): Uri? {
        return intent.data
    }
}
