package com.example.railsonalmeida.iddog.app.di

import com.example.railsonalmeida.iddog.view.register.di.*
import dagger.Component
import javax.inject.Singleton

/**
 * Created by railsonalmeida on 23/04/2018.
 */

@Singleton
@Component(modules = [(AppModule::class), (NetworkModule::class), (ApiModule::class)])
interface AppComponent {

    fun inject(module: RegisterModule): RegisterComponent

    fun inject(module: BreedsListModule): BreedsListComponent

}