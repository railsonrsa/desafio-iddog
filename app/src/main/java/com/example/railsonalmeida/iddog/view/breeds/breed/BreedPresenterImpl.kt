package com.example.railsonalmeida.iddog.view.register

import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListService
import com.example.railsonalmeida.iddog.view.register.interfaces.BreedsListView

/**
 * Created by railsonalmeida on 23/04/2018.
 */

class BreedPresenterImpl(val view: BreedsListView?, val service: BreedsListService)