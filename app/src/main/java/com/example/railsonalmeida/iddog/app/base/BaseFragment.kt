package com.example.railsonalmeida.iddog.app.base

import android.support.v4.app.Fragment

/**
 * Created by railsonalmeida on 23/04/2018.
 */

abstract class BaseFragment: Fragment()