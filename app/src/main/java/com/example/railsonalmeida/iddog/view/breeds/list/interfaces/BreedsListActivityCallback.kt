package com.example.railsonalmeida.iddog.view.breeds.list.interfaces

/**
 * Created by railsonalmeida on 29/04/2018.
 */

interface BreedsListActivityCallback {

    fun openBreedImage(imageUrl: String?)
}