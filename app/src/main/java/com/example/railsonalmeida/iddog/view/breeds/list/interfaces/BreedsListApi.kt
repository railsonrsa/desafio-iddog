package com.example.railsonalmeida.iddog.view.register.interfaces

import com.example.railsonalmeida.iddog.data.response.FeedResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

/**
 * Created by railsonalmeida on 23/04/2018.
 */

interface BreedsListApi {

    @GET("/feed")
    fun loadFeed(@Query("category") category: String?,
                 @Header("Authorization") token: String): Observable<FeedResponse>
}