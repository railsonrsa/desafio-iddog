package br.com.railsonrsa.nubank_hiring_exercise.schedulers

import io.reactivex.Scheduler

/**
 * Created by railsonalmeida on 23/04/2018.
 */
interface SchedulerProvider {

    fun ui(): Scheduler

    fun computation(): Scheduler

    fun trampoline(): Scheduler

    fun newThread(): Scheduler

    fun io(): Scheduler
}