package com.example.railsonalmeida.iddog.view.register

import br.com.railsonrsa.nubank_hiring_exercise.schedulers.TestSchedulerProvider
import com.example.railsonalmeida.iddog.data.pojo.User
import com.example.railsonalmeida.iddog.data.request.RegisterRequest
import com.example.railsonalmeida.iddog.data.response.ErrorResponse
import com.example.railsonalmeida.iddog.data.response.UserResponse
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterPresenter
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterService
import com.example.railsonalmeida.iddog.view.register.interfaces.RegisterView
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

/**
 * Created by railsonalmeida on 29/04/2018.
 */
@RunWith(MockitoJUnitRunner::class)
class RegisterFragmentTest {

    @Mock
    lateinit var view: RegisterView
    @Mock
    lateinit var service: RegisterService
    lateinit var presenter: RegisterPresenter
    lateinit var userResponse: UserResponse
    lateinit var registerRequest: RegisterRequest
    lateinit var errorResponse: ErrorResponse

    @Before
    fun setUp() {
        presenter = RegisterPresenterImpl(view, service, TestSchedulerProvider())
        presenter.setView(view)

        var user = User("email", "id", "token", Date(), Date(), 1)
        userResponse = UserResponse(user)

        registerRequest = RegisterRequest("test@email.com")
    }

    @Test
    fun userRegisterSuccessfully() {
        `when`(service.registerUser(registerRequest)).thenReturn(Observable.just(userResponse))
        presenter.registerUser("test@email.com")
        verify(view, times(1)).disableRegisterButton()
        verify(view, times(1)).startLoading()
        verify(view, times(1)).enableRegisterButton()
        verify(view, times(1)).stopLoading()
        verify(view, times(1)).onSuccessRegister()

    }

    @Test
    fun userRegisterEmptyEmail() {
        registerRequest.email = ""
        presenter.registerUser("")
        verify(view, times(1)).onInvalidEmail()
        verify(view, never()).disableRegisterButton()
        verify(view, never()).startLoading()
    }

}